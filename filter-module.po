#
# LANGUAGE translation of Drupal (modules/filter/filter.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from file: filter.module,v 1.160 2007/01/12 07:27:21 unconed
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-01-30 12:39+0100\n"
"PO-Revision-Date: 2007-02-22 21:37+0100\n"
"Last-Translator: Kokoška Juraj <juro@jk.sk>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=((n==1)?(0):(((n>=2)&&(n<=4))?(1):2));\n"

#: modules/filter/filter.module:23
msgid "The filter module allows administrators to configure  text input formats for the site. For example, an administrator may want a filter to strip out malicious HTML from user's comments. Administrators may also want to make URLs linkable even if they are only entered in an unlinked format."
msgstr "Modul filter umožňuje administrátorom nastaviť formáty vstupu. Administrátor napr. môže chcieť vyhodiť zákerné HTML z komentárov alebo upraviť URL na odkazy, aj keď sú zadané ako text."

#: modules/filter/filter.module:24
msgid "Users can choose between the available input formats when creating or editing content. Administrators can configure which input formats are available to which user roles, as well as choose a default input format. Administrators can also create new input formats. Each input format can be configured to use a selection of filters."
msgstr "Pri úprave alebo vytváraní obsahu si užívatelia môžu vybrať jeden z dostupných formátov vstupu. Administrátori môžu nastaviť, ktoré formáty sú dostupné ktorým skupinám užívateľov, a tiež určiť implcitný formát. Tiež môžu vytvoriť nové vstupné formáty. Každý formát sa skladá z niekoľko filtrov."

#: modules/filter/filter.module:25
msgid "For more information please read the configuration and customization handbook <a href=\"@filter\">Filter page</a>."
msgstr "Viacej informácií nájdete v príručke pre nastavenie a úpravy na <a href=\"@filter\">stránke Filter</a>."

#: modules/filter/filter.module:29
msgid ""
"\n"
"<p><em>Input formats</em> define a way of processing user-supplied text in Drupal. Every input format has its own settings of which <em>filters</em> to apply. Possible filters include stripping out malicious HTML and making URLs clickable.</p>\n"
"<p>Users can choose between the available input formats when submitting content.</p>\n"
"<p>Below you can configure which input formats are available to which roles, as well as choose a default input format (used for imported content, for example).</p>\n"
"<p>Note that (1) the default format is always available to all roles, and (2) all filter formats can always be used by roles with the \"administer filters\" permission even if they are not explicitly listed in the Roles column of this table.</p>"
msgstr ""
"\n"
"<p><em>Vstupné formáty</em> určujú zpôsob, ako sa spracováva text zadaný užívateľom. Každý formát má vlastné nastavenie použitých <em>filtrov</em>. Filtre môžu napríklad zaisťovať odstránenie nevhodného HTML alebo zvýraznenie URL.</p>\n"
"<p>Užívatelia si pri posielaní príspevkov môžu vybrať formát.</p>\n"
"<p>Nižšie si môžete nastaviť, ktoré formáty sú prístupné rôznym roliam užívateľov, a tiež nastaviť implicitný formát vstupu (používa sa napr. pre importovaný obsah).</p> <p>Poznámka: implicitný formát je k dispozícii vždy všetkým roliam a všetky filtre môžu vždy využiť role s právom \"administrácia filtrov\", aj keď nie sú zobrazené v stĺpci Role v tejto tabuľke.</p>"

#: modules/filter/filter.module:36
msgid ""
"\n"
"<p>Every <em>filter</em> performs one particular change on the user input, for example stripping out malicious HTML or making URLs clickable. Choose which filters you want to apply to text in this input format.</p>\n"
"<p>If you notice some filters are causing conflicts in the output, you can <a href=\"@rearrange\">rearrange them</a>.</p>"
msgstr ""
"\n"
"<p>Každý <em>filter</em> vykonáva jednu určitú zmenu užívateľského vstupu, napr. odstránenie HTML značiek alebo prevod URL na klikacie odkazy. Vyberte, ktoré filtre chcete použiť na text v tomto vstupnom formáte.</p>\n"
"<p>Ak zistíte, že niektoré filtre zpôsobujú konflikty vo výstupe, môžete <a href=\"@rearrange\">zmeniť ich poradie</a>.</p>"

#: modules/filter/filter.module:41
msgid "If you cannot find the settings for a certain filter, make sure you have enabled it on the <a href=\"@url\">view tab</a> first."
msgstr "Pokiaľ nemôžete nájsť nastavenie určitého filtra, presvedčte sa, či je zapnutý v záložke <a href=\"@url\">zoznam filtrov</a>."

#: modules/filter/filter.module:44
msgid ""
"\n"
"<p>Because of the flexible filtering system, you might encounter a situation where one filter prevents another from doing its job. For example: a word in an URL gets converted into a glossary term, before the URL can be converted in a clickable link. When this happens, you will need to rearrange the order in which filters get executed.</p>\n"
"<p>Filters are executed from top-to-bottom. You can use the weight column to rearrange them: heavier filters \"sink\" to the bottom.</p>"
msgstr ""
"\n"
"<p>Kvôli flexibilite filtrovacieho systému môžete naraziť na situáciu, keď jeden filter bráni v činnosti inému filtru. Napríklad: slovo z URL sa zmení na termín registra skôr, ako sa URL zmení na odkaz. Keď k tomu dôjde, musíte upraviť poradie, v akom sa filtre spúšťajú.</p>\n"
"<p>Filtre se vykonávajú zhora dole. Pomocou váhy ich môžete usporiadať: tažšie filtre sa prepadnú dole.</p>"

#: modules/filter/filter.module:58
msgid "Input formats"
msgstr "Vstupné formáty"

#: modules/filter/filter.module:59
msgid "Configure how content input by users is filtered, including allowed HTML tags, PHP code tags. Also allows enabling of module-provided filters."
msgstr "Nastaviť, ako je formatovaný vstup od užívateľov. To zahrňuje povolené HTML značky či PHP kód. Umožňuje zapnutie filtrov poskytovanými modulmi."

#: modules/filter/filter.module:71
msgid "Add input format"
msgstr "Pridať vstupný formát"

#: modules/filter/filter.module:79
msgid "Delete input format"
msgstr "Zmazať vstupný formát"

#: modules/filter/filter.module:86
msgid "Compose tips"
msgstr "Rady pre písanie"

#: modules/filter/filter.module:98
msgid "!format input format"
msgstr "formát vstupu '!format'"

#: modules/filter/filter.module:121
msgid "Rearrange"
msgstr "Preusporiadať"

#: modules/filter/filter.module:169
msgid ""
"\n"
"<p>This site allows HTML content. While learning all of HTML may feel intimidating, learning how to use a very small number of the most basic HTML \"tags\" is very easy. This table provides examples for each tag that is enabled on this site.</p>\n"
"<p>For more information see W3C's <a href=\"http://www.w3.org/TR/html/\">HTML Specifications</a> or use your favorite search engine to find other sites that explain HTML.</p>"
msgstr ""
"\n"
"<p>Tento web povoľuje HTML obsah. Možno vás desí učenie sa HTML, ale naučiť sa používať niekoľko málo najzákladnejších HTML \"značiek\" je veľmi ľahké. Táto tabuľka zobrazuje príklady pre každú značku povolenú na tomto webe.</p>\n"
"<p>Viacej informácií nájdete vo <a href=\"http://www.w3.org/TR/html/\">špecifikácii HTML</a> na W3C alebo si nájdite iné stránky, kde je HTML vysvetlené.</p>"

#: modules/filter/filter.module:173
msgid "Anchors are used to make links to other pages."
msgstr "Odkazy na iné stránky."

#: modules/filter/filter.module:174
msgid "By default line break tags are automatically added, so use this tag to add additional ones. Use of this tag is different because it is not used with an open/close pair like all the others. Use the extra \" /\" inside the tag to maintain XHTML 1.0 compatibility"
msgstr "Implicitne sa zalomenie riadkov vkladá automaticky, takže touto značkou pridáte ďalšie. Táto značka sa líši, pretože nemá otváraciu a ukončovaciu značku ako ostatné. Pre kompatibilitu s XHTML 1.0 vložte do značky naviac \" /\"."

#: modules/filter/filter.module:174
msgid "Text with <br />line break"
msgstr "Text so <br />zalomením riadku"

#: modules/filter/filter.module:175
msgid "By default paragraph tags are automatically added, so use this tag to add additional ones."
msgstr "Implicitne sa odstavce pridávajú automaticky, takže touto značkou pridáte ďalší."

#: modules/filter/filter.module:175
msgid "Paragraph one."
msgstr "Odstavec jedna."

#: modules/filter/filter.module:175
msgid "Paragraph two."
msgstr "Odstavec dva."

#: modules/filter/filter.module:176;176
msgid "Strong"
msgstr "Tučné"

#: modules/filter/filter.module:177;177
msgid "Emphasized"
msgstr "Zvýraznené"

#: modules/filter/filter.module:178;178
msgid "Cited"
msgstr "Citácia"

#: modules/filter/filter.module:179
msgid "Coded text used to show programming source code"
msgstr "Zobrazenie zdrojového kódu"

#: modules/filter/filter.module:179
msgid "Coded"
msgstr "Kód"

#: modules/filter/filter.module:180;180
msgid "Bolded"
msgstr "Tučné"

#: modules/filter/filter.module:181;181
msgid "Underlined"
msgstr "Podčiarknuté"

#: modules/filter/filter.module:182;182
msgid "Italicized"
msgstr "Kurzíva"

#: modules/filter/filter.module:183
msgid "Superscripted"
msgstr "Horný index"

#: modules/filter/filter.module:183
msgid "<sup>Super</sup>scripted"
msgstr "<sup>Horný</sup> index"

#: modules/filter/filter.module:184
msgid "Subscripted"
msgstr "Dolný index"

#: modules/filter/filter.module:184
msgid "<sub>Sub</sub>scripted"
msgstr "<sub>Dolný</sub> index"

#: modules/filter/filter.module:185;185
msgid "Preformatted"
msgstr "Predformátované"

#: modules/filter/filter.module:186
msgid "Abbreviation"
msgstr "Skratka"

#: modules/filter/filter.module:186
msgid "<abbr title=\"Abbreviation\">Abbrev.</abbr>"
msgstr "<abbr title=\"Skratka\">Skr.</abbr>"

#: modules/filter/filter.module:187
msgid "Acronym"
msgstr "Skratka"

#: modules/filter/filter.module:187
msgid "<acronym title=\"Three-Letter Acronym\">TLA</acronym>"
msgstr "<acronym title=\"Dvojznaková skratka\">DZ</acronym>"

#: modules/filter/filter.module:188;188
msgid "Block quoted"
msgstr "Odsadenie bloku"

#: modules/filter/filter.module:189;189
msgid "Quoted inline"
msgstr "Vložená citácia"

#: modules/filter/filter.module:191
msgid "Table"
msgstr "Tabuľka"

#: modules/filter/filter.module:191
msgid "Table header"
msgstr "Záhlavie tabuľky"

#: modules/filter/filter.module:191
msgid "Table cell"
msgstr "Bunka tabuľky"

#: modules/filter/filter.module:193;193
msgid "Deleted"
msgstr "Zmazané"

#: modules/filter/filter.module:194;194
msgid "Inserted"
msgstr "Vložené"

#: modules/filter/filter.module:196
msgid "Ordered list - use the &lt;li&gt; to begin each list item"
msgstr "Číslovaný zoznam - použite &lt;li&gt; na začiatku každej položky zoznamu"

#: modules/filter/filter.module:196;197
msgid "First item"
msgstr "Prvá položka"

#: modules/filter/filter.module:196;197
msgid "Second item"
msgstr "Druhá položka"

#: modules/filter/filter.module:197
msgid "Unordered list - use the &lt;li&gt; to begin each list item"
msgstr "Nečíslovaný zoznam - použite &lt;li&gt; na začiatku každej položky zoznamu"

#: modules/filter/filter.module:200
msgid "Definition lists are similar to other HTML lists. &lt;dl&gt; begins the definition list, &lt;dt&gt; begins the definition term and &lt;dd&gt; begins the definition description."
msgstr "Zoznam definície je podobný ostatným HTML zoznamom. &lt;dl&gt; začína zoznam definícii, &lt;dt&gt; začína termín a &lt;dd&gt; začína popis definície."

#: modules/filter/filter.module:200
msgid "First term"
msgstr "Prvý termín"

#: modules/filter/filter.module:200
msgid "First definition"
msgstr "Prvá definícia"

#: modules/filter/filter.module:200
msgid "Second term"
msgstr "Druhý termín"

#: modules/filter/filter.module:200
msgid "Second definition"
msgstr "Druhá definícia"

#: modules/filter/filter.module:202;203;204;205;206;207
msgid "Header"
msgstr "Nadpis"

#: modules/filter/filter.module:203
msgid "Subtitle"
msgstr "Podnadpis"

#: modules/filter/filter.module:204
msgid "Subtitle three"
msgstr "Podnadpis tri"

#: modules/filter/filter.module:205
msgid "Subtitle four"
msgstr "Podnadpis štyri"

#: modules/filter/filter.module:206
msgid "Subtitle five"
msgstr "Podnadpis päť"

#: modules/filter/filter.module:207
msgid "Subtitle six"
msgstr "Podnadpis šesť"

#: modules/filter/filter.module:209
msgid "Tag Description"
msgstr "Popis značky"

#: modules/filter/filter.module:209;238
msgid "You Type"
msgstr "Napíšete"

#: modules/filter/filter.module:209;238
msgid "You Get"
msgstr "Získate"

#: modules/filter/filter.module:223
msgid "No help provided for tag %tag."
msgstr "Chýba nápoveda pre značku %tag."

#: modules/filter/filter.module:229
msgid ""
"\n"
"<p>Most unusual characters can be directly entered without any problems.</p>\n"
"<p>If you do encounter problems, try using HTML character entities. A common example looks like &amp;amp; for an ampersand &amp; character. For a full list of entities see HTML's <a href=\"http://www.w3.org/TR/html4/sgml/entities.html\">entities</a> page. Some of the available characters include:</p>"
msgstr ""
"\n"
"<p>Väčšinu neobvyklých znakov môžete zadávať bez problémov.</p>\n"
"<p>Pokiaľ narazíte na problémy, skúste použiť HTML entity. Napr. zapíšte &amp;amp; pre znak ampersandu &amp;. Kompletný zoznam HTML entít nájdete na stránke <a href=\"http://www.w3.org/TR/html4/sgml/entities.html\">entity</a>. Medzi najpoužívanejšie patrí:</p>"

#: modules/filter/filter.module:233
msgid "Ampersand"
msgstr "Ampersand"

#: modules/filter/filter.module:234
msgid "Greater than"
msgstr "Väčší ako"

#: modules/filter/filter.module:235
msgid "Less than"
msgstr "Menší ako"

#: modules/filter/filter.module:236
msgid "Quotation mark"
msgstr "Úvodzovky"

#: modules/filter/filter.module:238
msgid "Character Description"
msgstr "Popis znaku"

#: modules/filter/filter.module:252
msgid "No HTML tags allowed"
msgstr "Nie sú povolené HTML značky"

#: modules/filter/filter.module:260
msgid "You may post PHP code. You should include &lt;?php ?&gt; tags."
msgstr "Môžete vkladať PHP kód. Mali by ste ho vložiť medzi značky  &lt;?php a ?&gt;"

#: modules/filter/filter.module:262
msgid ""
"\n"
"<h4>Using custom PHP code</h4>\n"
"<p>If you know how to script in PHP, Drupal gives you the power to embed any script you like. It will be executed when the page is viewed and dynamically embedded into the page. This gives you amazing flexibility and power, but of course with that comes danger and insecurity if you do not write good code. If you are not familiar with PHP, SQL or with the site engine, avoid experimenting with PHP because you can corrupt your database or render your site insecure or even unusable! If you do not plan to do fancy stuff with your content then you are probably better off with straight HTML.</p>\n"
"<p>Remember that the code within each PHP item must be valid PHP code - including things like correctly terminating statements with a semicolon. It is highly recommended that you develop your code separately using a simple test script on top of a test database before migrating to your production environment.</p>\n"
"<p>Notes:</p><ul><li>You can use global variables, such as configuration parameters, within the scope of your PHP code but remember that global variables which have been given values in your code will retain these values in the engine afterwards.</li><li>register_globals is now set to <strong>off</strong> by default. If you need form information you need to get it from the \"superglobals\" $_POST, $_GET, etc.</li><li>You can either use the <code>print</code> or <code>return</code> statement to output the actual content for your item.</li></ul>\n"
"<p>A basic example:</p>\n"
"<blockquote><p>You want to have a box with the title \"Welcome\" that you use to greet your visitors. The content for this box could be created by going:</p>\n"
"<pre>\n"
"  print t(\"Welcome visitor, ... welcome message goes here ...\");\n"
"</pre>\n"
"<p>If we are however dealing with a registered user, we can customize the message by using:</p>\n"
"<pre>\n"
"  global $user;\n"
"  if ($user->uid) {\n"
"    print t(\"Welcome $user->name, ... welcome message goes here ...\");\n"
"  }\n"
"  else {\n"
"    print t(\"Welcome visitor, ... welcome message goes here ...\");\n"
"  }\n"
"</pre></blockquote>\n"
"<p>For more in-depth examples, we recommend that you check the existing Drupal code and use it as a starting point, especially for sidebar boxes.</p>"
msgstr ""
"\n"
"<h4>Použitie vlastného PHP kódu</h4>\n"
"<p>Pokiaľ viete skriptovať v PHP, Drupal vám dává možnosť pridať skript, aký chcete. Spustí sa pri prezeraní stránky a dynamicky sa ku nej pripojí. Získáte tak ohromnú flexibilitu a silu, ale samozrejme tiež nebezpečie, ak napíšete chybný kód. Pokiaľ nie ste dobre oboznámený s PHP, SQL alebo engine systému, vyhnite sa experimentom s PHP, pretože môžete poškodiť databázu alebo spôsobiť zlé zobrazenie alebo dokonca bezpečnostné riziko vo vašom webe! Pokiaľ nemáte v pláne nejaké špeciality,  radšej si skúste vystačiť s obyčajným HTML.</p>\n"
"<p>Nezabudnite, že PHP kód musí být vždy správny - vrátane správných ukončení príkazov bodkočiarkami. Veľmi doporučujeme odladiť kód osobitne s použitím jednoduchého testovacieho skriptu a testovacej databázy skôr, ako ho spustíte v ostrej prevádzke.</p>\n"
"<p>Poznámky:</p><ul><li>V rozsahu svojho PHP kódu môžete používať globálne premenné (napr. konfiguráciu), ale pamätajte, že hodnota, ktorú priradíte globálnym premenným, im zostane aj neskôr pri spracovaní v engine systému.</li><li>register_globals je teraz štandardne nastavené na <strong>off</strong>. Informácie z formulárov získáte zo \"superglobálnych\" polí $_POST, $_GET atď.</li><li>Pre výstup obsahu môžete použiť buď <code>print</code> alebo <code>return</code>.</li></ul>\n"
"<p>Jednoduchý príklad:</p>\n"
"<blockquote><p>Chcete mať box s titulkom \"Vitajte\", ktorým pozdravíte návštevníkov. Obsah tohto boxu vytvoríte takto:</p>\n"
"<pre>\n"
" print t(\"Vitajte... tu patrí uvítanie...\");\n"
"</pre>\n"
"<p>Ak príde registrovaný užívateľ, môžeme uvítanie upraviť:</p>\n"
"<pre>\n"
" global $user;\n"
"  if ($user->uid) {\n"
"    print t(\"Vitajte $user->name, ...tu príde uvítanie ...\");\n"
"  }\n"
"  else {\n"
"    print t(\"Vitajte,... tu príde uvítanie ...\");\n"
"  }\n"
"</pre></blockquote>\n"
"<p>Podrobnejšie príklady nájdete v existujúcom kóde Drupalu, zo ktorého môžete vychádzať (hlavne pre postranné boxy).</p>"

#: modules/filter/filter.module:288
msgid "Lines and paragraphs break automatically."
msgstr "Riadky a odstavce sa zalomia automaticky."

#: modules/filter/filter.module:290
msgid "Lines and paragraphs are automatically recognized. The &lt;br /&gt; line break, &lt;p&gt; paragraph and &lt;/p&gt; close paragraph tags are inserted automatically. If paragraphs are not recognized simply add a couple blank lines."
msgstr "Automaticky sa rozpoznávajú riadky a odstavce. Automaticky sa vložia značky pre zalomenie riadku &lt;br /&gt;, začiatok &lt;p&gt; a koniec &lt;/p&gt; odstavca. Pokiaľ nie sú odstavce rozoznané, jednoducho pridajte pár prázdnych riadkov."

#: modules/filter/filter.module:294
msgid "Web page addresses and e-mail addresses turn into links automatically."
msgstr "Webové a e-mailové adresy sú automaticky zmenené na odkazy."

#: modules/filter/filter.module:318
msgid "All roles may use default format"
msgstr "Implicitný formát môžu použiť všetky role"

#: modules/filter/filter.module:318
msgid "No roles may use this format"
msgstr "Tento formát nemôžu používať žiadne role"

#: modules/filter/filter.module:323
msgid "Set default format"
msgstr "Nastaviť implicitný formát"

#: modules/filter/filter.module:330
msgid "Default format updated."
msgstr "Implicitný formát bol aktualizovaný."

#: modules/filter/filter.module:368
msgid "Are you sure you want to delete the input format %format?"
msgstr "Naozaj chcete odstrániť formát vstupu %format?"

#: modules/filter/filter.module:368
msgid "If you have any content left in this input format, it will be switched to the default input format. This action cannot be undone."
msgstr "Ak máte nejaký obsah v tomto formáte, prepne sa na implicitný formát vstupu. Tento krok už nemožno vrátiť späť."

#: modules/filter/filter.module:371
msgid "The default format cannot be deleted."
msgstr "Implicitný formát nie je možné zmazať."

#: modules/filter/filter.module:394
msgid "Deleted input format %format."
msgstr "Zmazaný vstupný formát %format."

#: modules/filter/filter.module:405
msgid "All roles for the default format must be enabled and cannot be changed."
msgstr "Implicitný formát musí byť povolený pre všetky role."

#: modules/filter/filter.module:412
msgid "Specify a unique name for this filter format."
msgstr "Zadajte jedinečný název pre tento formát filtra."

#: modules/filter/filter.module:419
msgid "Choose which roles may use this filter format. Note that roles with the \"administer filters\" permission can always use all the filter formats."
msgstr "Vyberte, ktoré role môžu používať tento filter. Poznámka: role s právom \"administrácia filtrov\" môžu vždy využívať všetky filtre."

#: modules/filter/filter.module:438
msgid "Filters"
msgstr "Filtre"

#: modules/filter/filter.module:439
msgid "Choose the filters that will be used in this filter format."
msgstr "Zvoľte filtre, ktoré sa použijú v tomto formáte."

#: modules/filter/filter.module:454;961
msgid "More information about formatting options"
msgstr "Viac informácií o možnostiach formátovania"

#: modules/filter/filter.module:457
msgid "No guidelines available."
msgstr "Nie sú k dispozícii žiadne rady."

#: modules/filter/filter.module:459
msgid "These are the guidelines that users will see for posting in this input format. They are automatically generated from the filter settings."
msgstr "Toto sú rady, ktoré užívatelia uvidia pri písaní v tomto vstupnom formáte. Automaticky sa generujú z nastavena filtrov."

#: modules/filter/filter.module:461;834
msgid "Formatting guidelines"
msgstr "Rady pre formátovanie"

#: modules/filter/filter.module:476
msgid "Filter format names need to be unique. A format named %name already exists."
msgstr "Názov filtra musí byť jedinečný. Formát %name už existuje."

#: modules/filter/filter.module:495
msgid "Added input format %format."
msgstr "Pridaný vstupný formát %format."

#: modules/filter/filter.module:498
msgid "The input format settings have been updated."
msgstr "Nastavenie vstupného formátu bolo aktualizované."

#: modules/filter/filter.module:585
msgid "The filter ordering has been saved."
msgstr "Poradie filtrov bolo uložené."

#: modules/filter/filter.module:609
msgid "No settings are available."
msgstr "Žiadne nastavenia nie sú k dispozícii."

#: modules/filter/filter.module:810
msgid "Input format"
msgstr "Vstupný formát"

#: modules/filter/filter.module:920
msgid "input formats"
msgstr "formáty vstupu"

#: modules/filter/filter.module:982;1036
msgid "HTML filter"
msgstr "HTML filter"

#: modules/filter/filter.module:982
msgid "PHP evaluator"
msgstr "PHP prekladač"

#: modules/filter/filter.module:982
msgid "Line break converter"
msgstr "Prekladač zalomení koncov riadkov"

#: modules/filter/filter.module:982;1096
msgid "URL filter"
msgstr "URL filter"

#: modules/filter/filter.module:990
msgid "Allows you to restrict if users can post HTML and which tags to filter out."
msgstr "Umožňuje obmedziť, či môžu užívatelia písať v HTML a ktoré tagy sa majú odfiltrovať."

#: modules/filter/filter.module:992
msgid "Runs a piece of PHP code. The usage of this filter should be restricted to administrators only!"
msgstr "Spúšťa časť PHP kódu. Použitie tohto filtra by malo byť obmedzené iba pre administrátorov!"

#: modules/filter/filter.module:994
msgid "Converts line breaks into HTML (i.e. &lt;br&gt; and &lt;p&gt; tags)."
msgstr "Prevádza odriadkovanie do HTML (tj. tagy &lt;br&gt; a &lt;p&gt;)."

#: modules/filter/filter.module:996
msgid "Turns web and e-mail addresses into clickable links."
msgstr "Prevedie webové a e-mailové adresy na klikacie odkazy."

#: modules/filter/filter.module:1041
msgid "Filter HTML tags"
msgstr "Filtrovať HTML značky"

#: modules/filter/filter.module:1043
msgid "Strip disallowed tags"
msgstr "Odstráňiť nepovolené značky"

#: modules/filter/filter.module:1043
msgid "Escape all tags"
msgstr "Použít escape sekvencie pre vešky tagy"

#: modules/filter/filter.module:1044
msgid "How to deal with HTML tags in user-contributed content. If set to \"Strip disallowed tags\", dangerous tags are removed (see below). If set to \"Escape tags\", all HTML is escaped and presented as it was typed."
msgstr "Ako pracovať s HTML tagy v užívateľskom texte. Ak je nastavené \"Odstraňovať nepovolené značky\", nebezpečné tagy sú odstránené (pozri nížšie). Ak je nastavené \"Použíť escape sekvencie\", všetky HTML tagy sú zobrazené tak, ako boli napísané."

#: modules/filter/filter.module:1052
msgid "If \"Strip disallowed tags\" is selected, optionally specify tags which should not be stripped. JavaScript event attributes are always stripped."
msgstr "Ak je zvolené \"Odstraňovať nepovolené značky\", môžete zadať povolené tagy. Javascriptové udalosti sa vymažú vždy."

#: modules/filter/filter.module:1056
msgid "Display HTML help"
msgstr "Zobraziť HTML nápovedu"

#: modules/filter/filter.module:1058
msgid "If enabled, Drupal will display some basic HTML help in the long filter tips."
msgstr "Ak je povolené, Drupal zobrazí v dlhších tipoch pre filtre základnú nápovedu k HTML."

#: modules/filter/filter.module:1062
msgid "Spam link deterrent"
msgstr "Zabrániť spamovým odkazom"

#: modules/filter/filter.module:1064
msgid "If enabled, Drupal will add rel=\"nofollow\" to all links, as a measure to reduce the effectiveness of spam links. Note: this will also prevent valid links from being followed by search engines, therefore it is likely most effective when enabled for anonymous users."
msgstr "Ak je pole zaškrtnuté, Drupal pridá ku všetkým odkazom rel=\"nofollow\", ktoré znižuje účinnosť spamových odkazov. Poznámka: vyhľadávače potom neprechádzajú ani cez platné odkazy, takže je najlepšie túto možnosť zapnúť iba pre filter pre anonymných užívateľov."

#: modules/filter/filter.module:1101
msgid "Maximum link text length"
msgstr "Maximálna dĺžka textu odkazu"

#: modules/filter/filter.module:1104
msgid "URLs longer than this number of characters will be truncated to prevent long strings that break formatting. The link itself will be retained; just the text portion of the link will be truncated."
msgstr "URL dhšie ako toto číslo budú zkrátené, aby sa predišlo dlhým textom, ktoré ničia formátovanie. Samotný odkaz bude zachovaný, zkráti sa len textová časť odkazu."

#: modules/filter/filter.module:139
msgid "administer filters"
msgstr "administrácia filtrov"

#: modules/filter/filter.module:0
msgid "filter"
msgstr "filter"

